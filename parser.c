#include "intstack.h"
#define true 1
#define false 0

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
//data
struct stack rx;
int rc;
char t_col[21][10]={"int",  //0
                    "main",//1
                    "begin",//2
                    "end",//3
                    ";",//4
                    "id",//5
                    "[",//6
                    "]",//7
                    "number",//8
                    "=",//9
                    "endfor",//10
                    "do",//11
                    "operator",//12
                    "to",//13
                    "for",//14
                    "endif",//15
                    "if",//16
                    "return",//17
                    "(",//18
                    ")",//19
                    "$"//20
                  };

char follow[4][10]={
  ";",
  "begin",
  "do",
  "$"
}, first[6][3][10]={
{"if","endif","F"},
{"int",";","F"},
{"for","endfor","F"},
{"return",")","F"},
{"F","none","E"},
{ "","","e"}
};
int t_size = sizeof(t_col)/sizeof(t_col[0]);
int parse_table[38][21]={
//0   1   2   3   4   5   6   7   8   9     10  11  12  13  14  15  16  17  18  19  20
 {0  ,0  ,0  ,1  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//0
 {0  ,0  ,2  ,0  ,4  ,0  ,0  ,0  ,0   ,0    ,8  ,0  ,0  ,0  ,0  ,19 ,0  ,0  ,0  ,33 ,0  },//1
 {0  ,3  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,32 ,0  },//2
 {5  ,0  ,2  ,0  ,0  ,0  ,0  ,0  ,0   ,4    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,36 ,0  ,0  },//3
 {0  ,0  ,0  ,0  ,0  ,3  ,0  ,6  ,3   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//4
 {0  ,0  ,2  ,0  ,4  ,0  ,0  ,0  ,0   ,0    ,8  ,0  ,0  ,0  ,0  ,19 ,0  ,0  ,0  ,33 ,-1 },//5
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,7   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//6
 {0  ,0  ,0  ,0  ,0  ,0  ,4  ,6  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//7
 {0  ,0  ,0  ,0  ,23 ,0  ,0  ,0  ,0   ,0    ,8  ,9  ,0  ,0  ,0  ,19 ,0  ,0  ,0  ,4  ,0  },//8
 {0  ,0  ,0  ,0  ,0  ,10 ,0  ,3  ,10  ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//9
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,11 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//10
 {0  ,0  ,0  ,0  ,0  ,12 ,0  ,6  ,12  ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//11
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,13 ,14 ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//12
 {0  ,0  ,0  ,0  ,0  ,13 ,0  ,0  ,13  ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//13
 {0  ,0  ,0  ,0  ,0  ,15 ,0  ,0  ,15  ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//14
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,16   ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//15
 {0  ,0  ,0  ,0  ,0  ,17 ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//16
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,18   ,0  ,0  ,0  ,0  ,1  ,0  ,0  ,0  ,0  ,0  ,0  },//17
 {0  ,0  ,0  ,0  ,0  ,17 ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//18
 {0  ,0  ,0  ,0  ,23 ,20 ,0  ,0  ,20  ,0    ,8  ,9  ,0  ,0  ,0  ,19 ,0  ,0  ,0  ,0  ,0  },//19
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,21 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//20
 {0  ,0  ,0  ,0  ,0  ,22 ,0  ,24 ,22  ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//21
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,8  ,0  ,0  ,0  ,0  },//22
 {0  ,0  ,0  ,0  ,0  ,27 ,0  ,24 ,27  ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//23
 {0  ,0  ,0  ,0  ,0  ,25 ,0  ,0  ,25  ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//24
 {0  ,0  ,0  ,0  ,0  ,0  ,26 ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//25
 {0  ,0  ,0  ,0  ,0  ,27 ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//26
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,28   ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//27
 {0  ,0  ,0  ,0  ,0  ,19 ,0  ,29 ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//28
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,30  ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//29
 {0  ,0  ,0  ,0  ,0  ,0  ,31 ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//30
 {0  ,0  ,0  ,0  ,0  ,19 ,0  ,0  ,0   ,0    ,0  ,9  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//31
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,2  ,0  ,0  },//32
 {0  ,0  ,0  ,0  ,0  ,34 ,0  ,0  ,34  ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//33
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,35 ,0  ,0  },//34
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ,0  ,0  ,0  },//35
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,8  ,0  ,0  ,0  },//36
 {0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0   ,0    ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  },//37
};


//functions

struct stack er;

int stoi(char * a){
  char c;
  int i=0,val=0;
  while((c=a[i++])!='\0'){
    if((c-'0')>9||(c-'0')<0) return -1;
  //  printf("%d\n",c-'0');
    val=val*10+(c-'0');
  }
  return val;
}

int getIndex(char * a){
  int i;
  for(i=0;i<t_size;i++){
    if(strcmp(t_col[i],a)==0)
      return i;
    }
  //printf("\nid:%d\n",a[0]-'0');
  if(a[0]=='-'||a[0]=='>'){
    return getIndex("operator");
  }
  //identifier
  if((a[0]-'0')>9||a[0]=='_')
      return getIndex("id");
  //number
  if(stoi(a)!=-1)
    return getIndex("number");
 return -1;
}

char t_col_type[21][20]={"keyword",  //0
                    "keyword",//1
                    "keyword",//2
                    "keyword",//3
                    "special char",//4
                    "identifier",//5
                    "special char",//6
                    "special char",//7
                    "number",//8
                    "operator",//9
                    "keyword",//10
                    "keyword",//11
                    "operator",//12
                    "keyword",//13
                    "keyword",//14
                    "keyword",//15
                    "keyword",//16
                    "keyword",//17
                    "special char",//18
                    "special char",//19
                    "eof"//20
                  };
char * getType(int x){
  return t_col_type[x];
}

int getNextState(int state,int index){
  return parse_table[state][index];
}

//isreduce()
int isReduce(char *p,char *q){
  int i=4;
  //if()
  //printf("\n\n%s\n%s",p,q );
  if(strcmp(p,"return")==0)return true;
  if((strcmp(p,"if")==0||strcmp(p,"return")==0||strcmp(p,"int")==0||strcmp(p,")")==0||strcmp(p,"for")==0||getIndex(p)==5)&&(strcmp(q,"endif")==0||strcmp(q,"endfor")==0))return true;

  while(i--){
    if(strcmp(q,follow[i])==0&&strcmp("end",p)!=0&&strcmp("endfor",p)!=0&&strcmp("endif",p)!=0||getIndex(p)==5&&getIndex(q)==5){
      //printf("\n--%s/%s\n",p,q );
      return true;
    }
  }
  return false;
}

char * getReduceTill(char * p){
  int i=4;
  while(i--){
    if(strcmp(p,first[i][0])==0){
      return first[i][1];
    }
  }
  if(getIndex(p)==5)return ";";
  return "";
}


char s[100];
int popTill(char * p,struct stack * st){
  char * q,*as,*e,pp[20]={'\0'},pq[30]={'\0'},*z;
  int i,f=1,len;
  er.top=-1;

  struct stack rev;
  rev.top=-1;
  strcpy(pq,s);
  for(i=rx.top;i>=0;i--){
  //  printf("\n%s",rx.stk[i]);
  }
  if(p!=NULL){
 while(strcmp((q=pop(st)),p)!=0&&st->top!=-1){
 //printf("\npopped : %s",q );
 push(&rev,q);
 strcat(s,"");

 if('F'==q[0])
 {
  // printf("test:%s\n",q );
    as = pop(&rx);
    strcat(s,e=pop(&rx));
    push(&er,e);
    push(&rx,as);
  }
 else
    strcat(s,t_col[getIndex(q)]);
 strcat(s," ");
 }
  push(&rev,q);

 }
 i=0;
 len = strlen(s);
 len = len - (er.top+1)*3;


 if(er.top>=1){
   while(er.top>=0){
     e=pop(&er);
     i+=1;
     sprintf(pp,"%s%s",pp,e);
    // printf("working : %s\n",pp );
     if(i%2==0){
       rc++;
       printf("\nF%d -> %s",rc,pp );
       sprintf(pp,"F%d",rc);
       push(&er,pp);

       strcpy(pp,"");

     }
   }
   //snprintf(z,2,"%s",s);
   s[len]='\0';

   sprintf(p,"%s %s ",s,e);
   strcpy(s,"");
   strcpy(s,p);
  // printf("\ndadasa:%s",s);


 }


 strcat(s,q);
 /*
 strcpy(s,pq);
// printf("fine\n" );
 for(i=0;i<=rev.top;i++){
   q=rev.stk[i];

   //printf("fine:%s\n",q );
   if('F'==q[0])
   {
    // printf("test:%s\n",q );
    if(f){
      sprintf(e,"F%d",rc);
      strcat(s,e);
      f=0;
      strcat(s," ");
    }
    }
   else
    { strcat(s,t_col[getIndex(q)]);
   strcat(s," ");
 }
   }
*/
 return st->top==-1&&strcmp(";",q)!=0?1:0;
}






void setVar(char *fir,char *la){
 int i=0;
 char *buff;

 if(getIndex(fir)==5&&strcmp(";",la)==0){
  // printf("\n%s:%s", fir,la);
  sprintf(s,"%s%d","F",rc++);
  push(&rx,s);
  return;
}

 for(i=0;i<5;i++){
   if(strcmp(fir,first[i][0])==0&&strcmp(la,first[i][1])==0){
    // printf("\n%s::::%s", fir,la);
     sprintf(s,"%s%d",first[i][2],rc++);
     push(&rx,s);
     break;
   }

 }

}

int parse(struct stack *st){

char *p,*q,r[100],t[10];
int state=0,i,sbt=0,pstate;
int f=0;
rc=0;
struct stack key,val;
//push(&input,";");
input.top=-1;
nest.top=-1;
rx.top=-1;

if(st->top==0){
  printf("\nE->(null)\ns -> E\n\n%s\n","ther is nothing in input file" );
  st->top=-1;
}

while(st->top!=-1){
 p = pop(st);
 if(st->top!=-1){

//printf("--------------\n\n\npushed %s\n",p );

push(&input,p);
q=pop(st);
//printf("next : %s",q);
push(st,q);
if(isReduce(p,q)){//implement

  printf("%s\n<Reduce>%s",KRED,KGRN );
  strcpy(s,"");
  setVar(p,getReduceTill(p));
  strcpy(t,s);
  strcat(s," -> ");
  strcpy(r,s);
  strcpy(s,"");


  if(popTill(getReduceTill(p),&input))
    printf("\nE -> %s",s);
  else
    printf("\n%s F %s",r,s);
    //  printf("\ntttt:%s\n",t );
  push(&input,t);
  f=1;

}

}

pstate=state;
state = getNextState(state,getIndex(p));
if(sbt){
  sbt=0;
  if(strcmp("]",p)!=0||getIndex(p)!=getIndex("id"));

}
 if(strcmp("endfor",p)==0||strcmp("endif",p)==0||strcmp("]",p)==0){
   ipush(&nest,pstate);
 }
 if(nest.top>=0){
   if(strcmp("for",p)==0||strcmp("if",p)==0||(i=strcmp("[",p))==0){
     if(i==0){
       sbt=1;
     }
state=ipop(&nest);
    //   printf("\nsymbol %s\tnextstate:%d\t\tindex:%d\n",p,state,getIndex(p));

     continue;
   }

 }
 printf("%s",KRED );
 if(getIndex(p)==-1||state==0){
   printf("\n\nsyntax error near %s\n\n", p);
 printf("symbol %s\tnextstate:%d\t\tindex:%d\n",p,state,getIndex(p));
   break;
 }
printf("%s",KWHT );
printf("\nsymbol %s%10s%s is a \t%s%15s%s\t\tnextstate:%d\tindex:%d",KMAG,p,KWHT,KYEL,getType(getIndex(p)),KWHT,state,getIndex(p));
}

/*
while(input.top>-1){
  strcpy(s,"");
  p=pop(&input);
//  printf("\nafter loop pop :%s\n",p );
  setVar(p,"none");
  strcat(s," -> ");
  push(&input,p);
  popTill(p,&input);
  printf("\n%s",s);
}*/

if(st->top==-1&&state==-1&&nest.top==-1){
  if(f)
  printf("\n%sF -> (null)",KGRN);
  printf("\nS -> E");
  printf("\n%saccepted\n",KWHT );
}
else{
  printf("\n%srejected\n",KRED );
}
return 0;
}
